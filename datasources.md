# Sources of data to inspire or to be used in the final project

## Specific requests from local companies

### PREDICT CUSTOMER MACHINE PURCHASES
* Data: a multi-year data set of machine owners who finance their machine purchases, showing the purchaser name, date, 
  machine info, financing organization, etc.
* Question: Can that data be used to estimate when each purchaser will  purchase the next piece of equipment to replace one 
  they previously bought. 
* Value: Such predictive information would allow us to improve efficiency in calling on customers by calling on them as they 
  enter the buying cycle. Some preliminary analysis may be available.
  

### PREDICT POWER OUTAGES 
* Context: A company coordinates rental power across the country. The current process includes waiting for the power to go out at 
  a facility, after which the facility manager reports the outage to emergency operations team, who notifies us to dispatch a generator set. 
  In some cases, the team anticipates areas of extreme weather and chooses to pre-position rental generator sets at strategic locations. 
* Question 1: Could historical data on watches and warnings from National Weather Service be used to develop profiles for each of 
  center locations, resulting in a risk profile or priority scoring system for national network that could provide guidance for 
  emergency planning.
* Question 2: Can a method be developed for using National Weather Service watches and warnings to predict when actual storms will hit 
  any given location with more notice than currently aailable. Such increased notice could allow our team to give advance warning to local 
  dealers about potential outages, allowing them to stage correctly sized generator sets and cables on their yards in anticipation of 
  rental requests. 
* Value: A pretty sophisticated model of failure modes and effects analysis


## Various projects

* [Locations of netflix servers](http://spectrum.ieee.org/tech-talk/telecom/internet/researchers-map-locations-of-4669-servers-in-netflixs-content-delivery-network)

* [Ways not to gather data](http://www.siliconbeat.com/2016/08/11/linkedin-bot-attack-thats-stealing-member-data/)

* [Stanford Large Network Dataset Collection](https://snap.stanford.edu/data/index.html)

* [Kaggle datasets](https://www.kaggle.com/datasets)

     * [Kaggle: How ISIS Uses Twitter](https://www.kaggle.com/kzaman/how-isis-uses-twitter)

* Final projects from past year: http://github.com/fdac15

* [ORIGAMI](http://hypothesis.ornl.gov/)

## REST APIs 

* Twitter, Facebook, etc, see Mining-the-Social-Web-2nd-Edition

* Weather: http://openweathermap.org/api

* [Breaches](https://haveibeenpwned.com/PwnedWebsites)

## A compilation of sources (a lot of overlap):
* http://blog.visual.ly/data-sources/

* https://www.quora.com/Where-can-I-find-large-datasets-open-to-the-public

* http://readwrite.com/2008/04/09/where_to_find_open_data_on_the

* http://www.smartdatacollective.com/bernardmarr/235366/big-data-20-free-big-data-sources-everyone-should-know

* http://blog.bigml.com/2013/02/28/data-data-data-thousands-of-public-data-sources/

* https://datafloq.com/public-data/

## Types of data that may be worth collecting for cybersecurity topics
* http://www.secureworks.com/resources/blog/5-valuable-contextual-data-sources-for-small-businesses/

* Honeypot: http://security.stackexchange.com/questions/10168/setting-up-a-honeypot

## More traditional data sources: spreadsheets
* Government: http://www.data.gov/

* Finance:
http://quant.stackexchange.com/questions/141/what-data-sources-are-available-online

* Data mining-related:
http://www.kdnuggets.com/datasets/index.html

* Health care:
http://www.ahrq.gov/research/data/dataresources/index.html

* Arts:
http://arts.gov/grants-organizations/research-art-works/publicly-available-data-sources
